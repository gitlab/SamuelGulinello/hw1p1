package edu.bu.ec504.hw1p1;

/**
 * AMaintains an approximate count of the number of distinct elements seen so far.
 */
public abstract class DistinctCounter {
    // FIELDS

    /**
     * The memory associated with the counter.
     */
    protected final FixedBitArray mem;

    // METHODS

    // ... CONSTRUCTORS

    /**
     * This constructor cannot be used.
     */
    private DistinctCounter() {mem = null;}

    /**
     * Constructs an DistinctCounter with a given size of memory.
     *
     * @param memBits The number of bits of memory available to the counter.
     */
    public DistinctCounter(int memBits) {
        mem = new FixedBitArray(memBits);
    }

    /**
     * Sets the memory of this DistinctCounter object to <code>initialMem</code>.
     *
     * @param initialMem The memory to set for this object.
     * It should be in the same format as the output of a call to {@link #currentState()}
     * for some DistinctCounter object.
     */
    public void setMem(FixedBitArray initialMem) {
        if (mem.len != initialMem.len)
            throw new IllegalArgumentException(
                "Trying to set memory with an incompatible argument.");
        else {
            // copy initialMem bits to this object
            mem.bits.clear();
            mem.bits.xor(initialMem.bits);
        }
    }

    // ... OPERATIONAL

    /**
     * Note a new element that is seen.
     *
     * @param newElement The element that was seen.
     */
    abstract void saw(String newElement);

    /**
     * @return A guess of the number of distinct elements that were seen.
     */
    abstract Integer numDistinct();

    // ... INFORMATIONAL

    /**
     * outputs the current state of the DistinctCounter
     */
    final public FixedBitArray currentState() {
        return mem;
    }
}
