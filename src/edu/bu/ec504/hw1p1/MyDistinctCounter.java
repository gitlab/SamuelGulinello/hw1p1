package edu.bu.ec504.hw1p1;

/**
 * This is a trivial modification!
 */
public class MyDistinctCounter extends DistinctCounter {

  /**
   * @inheritDoc
   */
  public MyDistinctCounter(int memBits) {
    super(memBits);
  }

  /**
   * @inheritDoc
   */
  @Override
  void saw(String newElement) {
    mem.set(Math.abs(newElement.hashCode()%(mem.len)));
  }

  /**
   * @inheritDoc
   */
  @Override
  Integer numDistinct() {
    return mem.bits.cardinality();
  }

  // NESTED CLASSES
  /**
   * A runtime exception indicating that a method has not yet been implemented.
   */
  public static class NotYetImplemented extends RuntimeException {
  }
}
