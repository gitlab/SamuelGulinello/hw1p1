package edu.bu.ec504.hw1p1;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Main {
    // constants
    static final int memSize = 10; // the number of bits backing the counter

    /**
     * Run {@link MyDistinctCounter} through the test strings provided, and report its results.
     * @param testStrings The strings to run by the {@link MyDistinctCounter}.
     */
    static void test(ArrayList<String> testStrings) {
        // Show the counter my test strings
        MyDistinctCounter tester = new MyDistinctCounter(memSize);
        Set<String> verifier = new HashSet<>();

        for (String test: testStrings) {
            tester.saw(test);
            verifier.add(test);
        }

        // Record the counter's state
        FixedBitArray savedState = tester.currentState();
        System.out.println("Counter state is: "+ savedState);

        // Output the guess
        MyDistinctCounter newFoo = new MyDistinctCounter(memSize);
        newFoo.setMem(savedState);
        System.out.println("guessed # of distinct strings: "+newFoo.numDistinct());
        System.out.println(" actual # of distinct strings: "+verifier.size());
    }

    /**
     * Runs a simple test, based on the example in the homework description.
     */
    public static void runSimpleTest() {
        System.out.println("Simple test:");
        test(new ArrayList<>(List.of("Bravo", "Alfa", "Charlie", "Kilo", "Charlie", "Alfa", "Bravo")));
    }

    /**
     * Runs a longer, more complicated test based on a supplied argument
     * @param arg The argument to use in the test.
     */
    public static void runLongerTest(String arg) {
        System.out.println("\n\n\n"+"Longer test:");
        final BigInteger two = new BigInteger("2");
        final BigInteger modulus = new BigInteger(1,arg.getBytes()); // makes up a modulus based on the supplied argument
        BigInteger base = two;
        ArrayList<String> longList = new ArrayList<>(10000);
        for (int ii=0; ii<10000; ii++) {
            longList.add(base.toString());
            base=base.multiply(two).mod(modulus);
        }
        test(longList);
    }

    public static void main(String[] args) {
        runSimpleTest();

        if (args.length<1)
            throw new Error("Please provide a command-line argument.");
        else
            runLongerTest(args[0]);
    }
}
